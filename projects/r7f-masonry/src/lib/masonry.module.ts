import { NgModule } from '@angular/core';
import { MasonryComponent } from './masonry.component';
import { MasonryBrickDirective } from './masonry-brick.directive';

const moduleDeclarations = [
  MasonryComponent,
  MasonryBrickDirective
]

@NgModule({
  declarations: moduleDeclarations,
  exports: moduleDeclarations
})
export class MasonryModule { }
