interface MutationWindow extends Window {
  MutationObserver: any;
  WebKitMutationObserver: any;
}

declare var window: MutationWindow;

import {
  AfterViewInit,
  Directive,
  ElementRef,
  forwardRef,
  Inject,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2
} from '@angular/core';
import { MasonryComponent } from "./masonry.component";

@Directive({
  selector: '[masonry-brick], masonry-brick'
})
export class MasonryBrickDirective implements OnInit, OnDestroy, AfterViewInit {

  constructor(
    private renderer: Renderer2,
    private element: ElementRef,
    @Inject(forwardRef(() => MasonryComponent)) private parent: MasonryComponent,
    private zone: NgZone
  ) {
  }

  ngAfterViewInit(): void {
    const images = this.element.nativeElement.getElementsByTagName('img');
    if (images?.length > 0) {
      for (let imageRef of images) {
        this.renderer.listen(imageRef, 'load', (e) => {
          this.parent.add(this.element.nativeElement);
        })
        this.renderer.listen(imageRef, 'error', (e) => {
          this.parent.add(this.element.nativeElement);
        })
      }
    } else {
      this.parent.add(this.element.nativeElement);
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.parent.remove(this.element.nativeElement);
    this.zone.runOutsideAngular(this.watchForHtmlChanges.bind(this));
  }

  /** When HTML in brick changes dinamically, observe that and change layout */
  private watchForHtmlChanges(): void {
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    if (MutationObserver) {
      /** Watch for any changes to subtree */
      const self = this;
      const observer = new MutationObserver((mutations, observerFromElement) => {
        self.parent.layout();
      });

      // define what element should be observed by the observer
      // and what types of mutations trigger the callback
      observer.observe(this.element.nativeElement, {
        subtree: true,
        childList: true
      });
    }
  }
}
