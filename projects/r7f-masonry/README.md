# [Masonry](https://github.com/desandro/masonry) module for newest Angular versions

[![npm version](https://badge.fury.io/js/r7f-masonry.svg)](https://badge.fury.io/js/r7f-masonry)

> So this is a fork from https://github.com/jelgblad/angular2-masonry.  
> I'm currently trying to improve it with single dependency module for you, and well tested decision  
> Feel free to install and try it out, but depend on it at your own risk. Also, issues opened for you too.  
> Contributing guide will be soon

## Installation

`npm install r7f-masonry --save`

## Usage

Import `MasonryModule` into your app's modules:

``` typescript
import { MasonryModule } from 'r7f-masonry';

@NgModule({
  imports: [
    MasonryModule
  ]
})
```

```typescript
 @Component({
   selector: 'my-component',
   template: `
     <masonry>
       <masonry-brick class="brick" *ngFor="let brick of bricks">{{brick.title}}</masonry-brick>
     </masonry>
     `,
     styles: [`
       .brick { width: 200px; }
     `]
 })
 class MyComponent {
   bricks = [
     {title: 'Brick 1'},
     {title: 'Brick 2'},
     {title: 'Brick 3'},
     {title: 'Brick 4'},
     {title: 'Brick 5'},
     {title: 'Brick 6'}
   ]
 }
 ```
 
## Configuration

### Options
Read about Masonry options here: http://masonry.desandro.com/options.html

The `options`-attribute takes an object with the following properties:
* itemSelector: string;
* columnWidth: number | string;
* gutter: number;
* percentPosition: boolean;
* stamp: string;
* fitWidth: boolean;
* originLeft: boolean;
* originTop: boolean;
* containerStyle: string;
* transitionDuration: string;
* resize: boolean;
* initLayout: boolean;

#### Examples

Inline object:
```html
<masonry [options]="{ transitionDuration: '0.8s' }"></masonry>
```

From parent component:
```typescript
import { MasonryOptions } from 'r7f-masonry';

public masonryOptions: MasonryOptions = { 
  transitionDuration: '0.8s',
  resize: true 
};
```
```html
<masonry [options]="myOptions"></masonry>
```

### imagesLoaded
>**NOTE:** Will throw error if global `imagesLoaded` not available.

Delay adding brick until all images in brick are loaded.
To activate imagesLoaded set `useImagesLoaded` to `true`.
```html
<masonry [useImagesLoaded]="true"></masonry>
```

## Events
### layoutComplete: `EventEmitter<any[]>`
Triggered after a layout and all positioning transitions have completed.
>http://masonry.desandro.com/events.html#layoutcomplete

### removeComplete: `EventEmitter<any[]>`
Triggered after an item element has been removed.
>http://masonry.desandro.com/events.html#removecomplete

### Example
```html
<masonry (layoutComplete)="doStuff($event)" (removeComplete)="doOtherStuff($event)"></masonry>
```

## Demo
* Stackblitz: https://stackblitz.com/edit/r7f-masonry-demo
* Demo project: https://github.com/rovergulf/r7f-masonry/projects/demo

## Roadmap
 - [x] Angular 9 Support
 - [ ] Tests
 - [ ] Known bug with `columnWidth` option with string percentage value, probably should be issued to desandro's repo or I should contribute
 - [ ] Default options (at least I should think about it)
