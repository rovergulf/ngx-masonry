import { Component, OnInit } from '@angular/core';
import { MasonryOptions } from "../../../r7f-masonry/src/public-api";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  imagesUrl: string = 'https://s3.amazonaws.com/sample-v1';

  list = ['architecture', 'boat', 'bonding', 'books', 'family', 'overlooking', 'overview', 'river', 'rocks', 'wedding'];

  images: any[] = [];

  public masonryOptions: MasonryOptions = {
    resize: true,
    transitionDuration: '0s',
    // gutter: 10,
    itemSelector: '.brick', // <-- this is recommended,
  }

  constructor() {
  }

  ngOnInit(): void {
    this.images = this.list.map(name => `${this.imagesUrl}/${name}.jpg`);
  }

}
