#!/usr/bin/env bash

ng build r7f-masonry --prod
cd dist/r7f-masonry
npm publish --access public
cd ../..
